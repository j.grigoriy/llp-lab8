#include <stdio.h>
#include <sys/resource.h>
#include <stdint.h>
#include "image.h"
#include "bmp_utils.h"
#include "sepia_c.h"
#include "sepia_sse.h"

void invoke_function(void (*function)(struct image*, struct image*), struct image*, struct image*, char* description);
void check_for_default_img();
void check_for_big_img();

int main() {
    check_for_default_img();
    check_for_big_img();
    return 0;
}

void check_for_default_img() {
    struct image default_img;
    struct bmp_header header;
    struct image result1;
    struct image result2;
    puts("Check for default image");
    from_filename_bmp("../in.bmp", &default_img, &header);
    invoke_function(sepia_c, &default_img, &result1, "C");
    header = create_header_to_image(result1, BMP_LITTLE_ENDIAN);
    write_filename_bmp("out_default_c.bmp", result1, header);
    invoke_function(sepia_sse, &default_img, &result2, "SSE");

    header = create_header_to_image(result2, BMP_LITTLE_ENDIAN);
    write_filename_bmp("out_default_sse.bmp", result2, header);
    puts("End of checking default image\n");
}

void check_for_big_img() {
    struct image result1;
    struct image result2;
    struct image big_img;
    struct bmp_header header;

    big_img.width = 10000;
    big_img.height = 10000;
    big_img.data = malloc(big_img.width * big_img.height * sizeof(struct pixel));
    puts("Constructing big image");
    for (uint64_t x = 0; x < big_img.width; ++x) {
        for (uint64_t y = 0; y < big_img.height; ++y) {
            struct pixel rnd_pixel = {rand(), rand(), rand()};
            set_pixel(&big_img, x, y, rnd_pixel);
        }
    }
    puts("Big image construction finished\n");

    invoke_function(sepia_c, &big_img, &result1, "C");
    header = create_header_to_image(result1, BMP_LITTLE_ENDIAN);
    write_filename_bmp("out_big_c.bmp", result1, header);

    invoke_function(sepia_sse, &big_img, &result2, "SSE");

    header = create_header_to_image(result2, BMP_LITTLE_ENDIAN);
    write_filename_bmp("out_big_sse.bmp", result2, header);
}

void invoke_function(void (*function)(struct image*, struct image*), struct image* img_in, struct image* img_out, char* description) {
    struct rusage r;
    struct timeval start;
    struct timeval end;
    getrusage(RUSAGE_SELF, &r);
    start = r.ru_utime;

    function(img_in, img_out);

    getrusage(RUSAGE_SELF, &r);
    end = r.ru_utime;

    long res_time = ((end.tv_sec - start.tv_sec) * 1000000L) + end.tv_usec - start.tv_usec;
    printf("Time elapsed in seconds for %s filter: %.2f\n", description, res_time/1000000.0);

}
