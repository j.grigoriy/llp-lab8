#ifndef LAB8_SEPIA_SSE_H
#define LAB8_SEPIA_SSE_H

#include <stdlib.h>
#include "image.h"

void sepia_sse(struct image*, struct image*);

#endif //LAB8_SEPIA_SSE_H
