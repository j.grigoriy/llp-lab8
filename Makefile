C_SRC_K = -g -c
ASM_SRC_K = -fmacho64 -g

.PHONY: all

all:
	rm -rf bin
	mkdir bin
	gcc $(C_SRC_K) image.c -o bin/image.o
	gcc $(C_SRC_K) bmp_utils.c -o bin/bmp_utils.o
	gcc $(C_SRC_K) sepia_c.c -o bin/sepia_c.o
	gcc $(C_SRC_K) sepia_sse.c -o bin/sepia_sse.o
	nasm $(ASM_SRC_K) sepia_asm.asm -o bin/sepia_asm.o
	gcc $(C_SRC_K) main.c -o bin/main.o
	gcc -g bin/*.o -o bin/main
	rm -rf bin/*.o

