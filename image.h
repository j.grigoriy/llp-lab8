#ifndef LAB8_IMAGE_H
#define LAB8_IMAGE_H

#include <inttypes.h>

struct __attribute__((packed))pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel* data;
};

void set_pixel(const struct image* img, uint64_t x, uint64_t y, struct pixel p);
struct pixel get_pixel(const struct image img, uint64_t x, uint64_t y);
struct pixel* get_pixel_p(const struct image img, uint64_t x, uint64_t y);

#endif //LAB8_IMAGE_H
