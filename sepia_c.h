#ifndef LAB8_SEPIA_C_H
#define LAB8_SEPIA_C_H

#include <stdlib.h>
#include "image.h"

void sepia_c( struct image* img, struct image* res ) ;

#endif //LAB8_SEPIA_C_H
